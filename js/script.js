// ТЕОРЕТИЧНІ ПИТАННЯ
console.group("ТЕОРЕТИЧНІ ПИТАННЯ");



/* 
1. Як можна оголосити змінну у Javascript?
——————————————————————————————————————————*/
// За допомогою ідентифікаторів операторів const, let або var. Наприклад:
// let $admin;
// const numberOfTheBeast = 666; // бо неможна не ініціалізувати const
// var _uzvar;



/* 
2. У чому різниця між функцією prompt та функцією confirm?
——————————————————————————————————————————————————————————*/
// prompt() намагається отримати від користувача string
// (або null, якщо юзер натисне не ОК, а Cancel).
//
// confirm() намагається отримати від користувача Boolean:
// якщо юзер тисне OK —  скрипт отримує true, 
// якщо юзер тисне Cancel — скрипт отримує false.



/* 
3. Що таке неявне перетворення типів? Наведіть один приклад.
————————————————————————————————————————————————————————————
*/
// Це таке перетворення типу даних, коли тип змінюється
// не функцією, спеціально призначеною для перетворення типу, 
// а як побічний ефект операції. 
// Приклад неявного перетворення:
let a = 7; // маємо тип number
a = a + ""; // переробили number на string
console.log("шо за тип? " + typeof a) // довели, що переробили




// Кінець теоретичного блоку 
console.groupEnd();




// ЗАВДАННЯ
console.group("ПРАКТИЧНІ ЗАВДАННЯ");


// 1. Оголосіть дві змінні: admin та name. 
// Встановіть ваше ім'я в якості значення змінної name. 
// Скопіюйте це значення в змінну admin і виведіть його в консоль.

console.group("ЗАВДАННЯ 1");

let admin, name;
name = "Eddy";
admin = name;
console.log(admin);
document.getElementById("answer1").innerHTML = admin;

console.groupEnd();



// 2. Оголосити змінну days і ініціалізувати її 
// числом від 1 до 10. Перетворіть це число 
// на кількість секунд і виведіть на консоль.

console.group("ЗАВДАННЯ 2");

let days = 9;
let seconds = days * 24 * 3600;
console.log(seconds);
document.getElementById("answer2").innerHTML = seconds;

console.groupEnd();




// 3. Запитайте у користувача якесь значення і виведіть його в консоль.

console.group("ЗАВДАННЯ 3");

// все в один рядок:
// console.log(prompt("Користувач, будь ласка, введіть якесь значення:", "RKECb_3HA4EHHR"));

// Або можна вивести користувацьке значення і в консоль, і на сторінку:
const userValue = prompt("Користувач, будь ласка, введіть якесь значення:","RKECb_3HA4EHHR");
console.log(userValue);

document.getElementById("answer3").innerHTML = userValue;

console.groupEnd();
console.groupEnd();

console.debug("END OF HOMEWORK.");